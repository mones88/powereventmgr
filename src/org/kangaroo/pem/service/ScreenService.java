package org.kangaroo.pem.service;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EService;
import org.androidannotations.annotations.Receiver;
import org.kangaroo.pem.PowerEventMgrApp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

@EService
public class ScreenService extends Service {

    private static final String TAG = ScreenService.class.getSimpleName();

    @App PowerEventMgrApp app;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent arg0) {

        return null;
    }

    @Receiver(actions = Intent.ACTION_SCREEN_ON)
    void onScreenOn() {

        Log.i(TAG, "ACTION_SCREEN_ON");

        app.wakeUp();

        stopSelf();
    }

}
