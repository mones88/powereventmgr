package org.kangaroo.pem.activity;

import java.io.BufferedOutputStream;

import lombok.Cleanup;
import lombok.val;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EActivity;
import org.kangaroo.pem.PowerEventMgrApp;
import org.kangaroo.pem.R;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

@EActivity
public class PreferencesActivity extends PreferenceActivity {

    private static final String TAG = PreferencesActivity.class.getSimpleName();

    @App PowerEventMgrApp app;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (!MainActivity.IN_TASK_STACK) {
            val actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setHomeButtonEnabled(false);
        }

        getFragmentManager().beginTransaction().replace(android.R.id.content, new PrefsFragment()).commit();
    }

    public class PrefsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {

        private SharedPreferences prefs;

        private Preference bgPref;

        @Override
        public void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);

            prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

            prefs.edit()
                    .putBoolean("fixed_install_mode", app.readSettingsFile("usbhost_fixed_install_mode"))
                    .putBoolean("fastcharge_in_host_mode", app.readSettingsFile("usbhost_fastcharge_in_host_mode"))
                    .putBoolean("firm_sleep", app.readSettingsFile("usbhost_firm_sleep"))
                    .putBoolean("lock_usbdisk", app.readSettingsFile("usbhost_lock_usbdisk"))
                    .commit();

            addPreferencesFromResource(R.xml.preferences);

            prefs.registerOnSharedPreferenceChangeListener(this);

            bgPref = findPreference("sleep_background_type");
            bgPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {

                    try {
                        val intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("file/*");
                        intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[] { "image/*", "video/*" });
                        startActivityForResult(Intent.createChooser(intent, "Select file"), 1002);
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(getActivity(), "Please install a File Manager.", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }

            });
            bgPref.setSummary(prefs.getString("sleep_background_type", "No file selected"));
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {

            if (resultCode == RESULT_OK) {

                val uri = data.getData();

                grantUriPermission(getPackageName(), uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);

                String type = getContentResolver().getType(uri);
                if (type == null) {
                    type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(data.getDataString()));
                }

                try {
                    @Cleanup
                    val in = getContentResolver().openInputStream(uri);

                    @Cleanup
                    val out = new BufferedOutputStream(openFileOutput("sleepscreen", MODE_PRIVATE));

                    int read = 0;
                    for (byte[] bytes = new byte[1024]; (read = in.read(bytes)) != -1;) {
                        out.write(bytes, 0, read);
                    }
                } catch (Exception e) {
                    Log.e(TAG, "", e);
                }

                prefs.edit().putString("sleep_background_type", type).commit();
                bgPref.setSummary(prefs.getString("sleep_background_type", "No file selected"));
            }
        }

        @Override
        public void onDestroy() {

            prefs.unregisterOnSharedPreferenceChangeListener(this);

            super.onDestroy();
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {

            if ("enable_notification".equals(key) || "enable_status_bar_icon".equals(key) || "skip_countdown".equals(key)) {
                if (prefs.getBoolean("enable_notification", true)) {
                    app.showNotification();
                } else {
                    app.hideNotification();
                }
            } else if ("fixed_install_mode".equals(key)) {
                app.writeSettingsFile("usbhost_" + key, prefs.getBoolean(key, false));
            } else if ("fastcharge_in_host_mode".equals(key)) {
                app.writeSettingsFile("usbhost_" + key, prefs.getBoolean(key, false));
            } else if ("firm_sleep".equals(key)) {
                app.writeSettingsFile("usbhost_" + key, prefs.getBoolean(key, false));
            } else if ("lock_usbdisk".equals(key)) {
                app.writeSettingsFile("usbhost_" + key, prefs.getBoolean(key, false));
            }
        }

    }

}
